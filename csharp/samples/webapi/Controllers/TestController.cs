﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class TestController : ControllerBase
    {
        /// <summary>
        /// 相加
        /// </summary>
        /// <param name="x">第一个值</param>
        /// <param name="y">第二个值</param>
        /// <returns>和</returns>
        [HttpGet]
        public int Add(int x, int y)
        {
            return x + y;
        }

        [HttpGet]
        public int Divide(int x, int y)
        {
            return x - y;
        }
    }
}
