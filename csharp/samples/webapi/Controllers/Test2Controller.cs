﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    [Route("[controller]/[action]")]
    public class Test2Controller: ControllerBase
    {

        /// <summary>
        /// add2
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        [HttpGet]
        public int Add(int x,int y)
        {
            return x + y;
        }
    }
}
