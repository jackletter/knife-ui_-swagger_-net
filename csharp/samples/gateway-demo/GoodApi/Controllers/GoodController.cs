﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoodApi.Controllers
{
    /// <summary>
    /// 商品控制器
    /// </summary>
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class GoodController : ControllerBase
    {
        private readonly ILogger<GoodController> _logger;

        public GoodController(ILogger<GoodController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 获取新的商品编号
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        [HttpPost]
        public string CreateGoodNo([FromForm]string prefix) => $"{prefix}-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}";
    }
}
